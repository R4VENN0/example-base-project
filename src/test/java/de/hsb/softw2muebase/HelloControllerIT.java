package de.hsb.softw2muebase;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class HelloControllerIT {

    @Autowired
    MockMvc mockMvc;

    @Test
    void returnsNameWithBaseStatement() throws Exception {
        mockMvc
                .perform(get("/greeting/Seyling"))
                .andExpect(status().isOk())
                .andExpect(content().string("Guten Tag Seyling"));
    }

    @Test
    void returnsNameWithExtraStatement() throws Exception {
        mockMvc
                .perform(get("/greeting/Athene"))
                .andExpect(status().isOk())
                .andExpect(content().string("Guten Tag wuensche ich Ihnen, Athene"));
    }
}