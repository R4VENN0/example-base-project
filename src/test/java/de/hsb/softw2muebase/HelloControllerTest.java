package de.hsb.softw2muebase;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class HelloControllerTest {

    private final HelloController helloController = new HelloController();

    @Test
    void returnContainsGutenTag() {
        String expected = "Guten Tag";

        String result = helloController.greeting("Irgendwas");

        assertThat(result).contains(expected);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Andi, Angela, Olaf"})
    void returnContainsInput(String input) {
        String result = helloController.greeting(input);

        assertThat(result).contains(input);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Andi, Angela, Olaf"})
    void returnContainsCompleteStatement(String input) {
        String expected = "Guten Tag wuensche ich Ihnen, " + input;

        String result = helloController.greeting(input);

        assertThat(result).contains(expected);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Romeo, Julia, Skollan"})
    void returnContainsOnlyBaseStatement(String input) {
        String expected = "Guten Tag " + input;

        String result = helloController.greeting(input);

        assertThat(result).contains(expected);
    }
}